/// <reference path="../../src/index.ts" />


import {bootstrap} from "@angular/platform-browser-dynamic";
import {Component} from "@angular/core";
import {SAMPLE_COMPONENT_DIRECTIVES} from "../../src/index";
 
@Component({
    selector: "app",
    template: (`
    	<meu-componente [text]="'Hello'" [text2]="'World!'"></meu-componente>
	`),
    directives: [SAMPLE_COMPONENT_DIRECTIVES]
})
class Application {
 
}


bootstrap(Application);