export * from "./meu-componente.component";
export * from "./outro-componente.component";

import {MeuComponenteComponent} from "./meu-componente.component";
import {OutroComponenteComponent} from "./outro-componente.component";

export var SAMPLE_COMPONENT_DIRECTIVES = [
	MeuComponenteComponent,
	OutroComponenteComponent
];