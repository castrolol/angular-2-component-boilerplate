import {Component, Input} from "@angular/core";


@Component({
	selector: 'outro-componente',
	template: (`
		<h1>{{text}}</h1>
		<h2>{{text2}}</h2>
	`)
})
export class OutroComponenteComponent {
 
	@Input()
	text: String = "Work it!"

	@Input()
	text2: String = "Work it!"

}