import {Component, Input} from "@angular/core";


@Component({
	selector: 'meu-componente',
	template: (`
		<h1>{{text}}</h1>
		<h2>{{text2}}</h2>
	`)
})
export class MeuComponenteComponent {
 
	@Input()
	text: String = "Work it!"

	@Input()
	text2: String = "Work it!"

}